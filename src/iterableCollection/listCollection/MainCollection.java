package iterableCollection.listCollection;

import iterableCollection.model.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class MainCollection {
    public static void main(String[] args) {
        Collection<String> names = new ArrayList<>();
        //Add
        names.add("Richi");
        names.add("Umy");
        names.addAll(List.of("Sri", "Ahmad", "Kezia"));

        //Remove
        names.remove("Umy");
        names.removeAll(Arrays.asList("Sri","Kezia"));

        //Check
        System.out.println(names.contains("Umy"));
        System.out.println(names.containsAll(Arrays.asList("Richi","Kezia")));
        System.out.println(names.containsAll(Arrays.asList("Richi","Ahmad")));

        // loop data
        for (String name: names){
            System.out.println(name);
        }

        Collection<Person> persons = new ArrayList<>();
        persons.add(new Person(1,"Adi","Padang"));
        persons.add(new Person(2,"Yusuf","Medan"));
        persons.add(new Person(3,"Mithoha","Bandung"));
        persons.addAll( Arrays.asList(new Person(4,"Minantu","Bandung")));
        persons.addAll( Arrays.asList(
                new Person(5,"Aceng","Tasikmalaya"),
                new Person(6,"David","Medan")
        ));

//        persons.remove(new Person(1,"Adi","Padang"));
//        persons.removeAll(Arrays.asList(
//                new Person(2,"Yusuf","Medan"),
//                new Person(6,"David","Medan")
//        ));
//
//        System.out.println(persons.contains(1));

        for (Person p: persons){
            System.out.println(p);
        }
    }

}
